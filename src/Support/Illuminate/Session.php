<?php

namespace Garbee\Cart\Support\Illuminate;

use Garbee\Cart\Contracts\SessionInterface;
use Illuminate\Session\Store;

class Session implements SessionInterface
{

    /**
     * The primary key for the session.
     *
     * @var string
     */
    private $key;

    /**
     * The instance of the cart.
     *
     * @var string
     */
    private $instance;

    /**
     * Session store object.
     *
     * @var \Illuminate\Session\Store
     */
    protected $session;

    /**
     * Creates a new Illuminate based Session.
     *
     * @param  \Illuminate\Session\Store $session
     * @param  string $key
     * @param  string $instance
     */
    public function __construct(
        Store $session,
        string $key,
        string $instance
    )
    {
        $this->session = $session;
        $this->key = $key;
        $this->instance = $instance;
    }

    public function key(): string
    {
        return $this->key;
    }

    public function instance(): string
    {
        return $this->instance;
    }

    public function retrieve()
    {
        return $this->session->get($this->storeKey());
    }

    public function put($value)
    {
        $this->session->put($this->storeKey(), $value);
    }

    public function exists(): bool
    {
        return $this->session->has($this->storeKey());
    }

    public function destroy()
    {
        $this->session->forget($this->storeKey());
    }

    private function storeKey(): string
    {
        return $this->key . '.' . $this->instance;
    }
}
