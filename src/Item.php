<?php

namespace Garbee\Cart;

use Garbee\Cart\Contracts\Item as Contract;
use Money\Money;

class Item implements Contract
{
    /** @var string $identifier */
    private $identifier;

    /** @var string $name */
    private $name;

    /** @var int $quantity */
    private $quantity;

    /** @var Money $price */
    private $price;

    /**
     * Item constructor.
     * @param string $identifier
     * @param string $name
     * @param int $quantity
     * @param Money $price
     */
    public function __construct(
        string $identifier,
        string $name,
        int $quantity,
        Money $price
    )
    {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    public function identifier(): string
    {
        return $this->identifier;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function price(): Money
    {
        return $this->price;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }
}
