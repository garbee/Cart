<?php

namespace Garbee\Cart\Exceptions;

use Exception;

class ItemNotFound extends Exception
{
    protected $message = 'Item not found in the cart.';

    public $identifier;
}
