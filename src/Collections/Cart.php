<?php

namespace Garbee\Cart\Collections;

use Money\Money;
use Money\Currency;
use Garbee\Cart\Contracts\Item;
use Illuminate\Support\Collection;
use Garbee\Cart\Exceptions\ItemNotFound;
use Garbee\Cart\Contracts\CartCollection;

class Cart extends Collection implements CartCollection
{
    /** @var \Garbee\Cart\Contracts\Cart $cart */
    private $cart;

    public function add(Item $item): CartCollection
    {
        $this->removeIfFound($item->identifier());
        $this->put($item->identifier(), $item);

        return $this;
    }

    public function remove(string $identifier): CartCollection
    {
        $error = new ItemNotFound();
        $error->identifier = $identifier;
        $this->throwIf($error, !$this->has($identifier));

        $this->forget($identifier);

        return $this;
    }

    public function quantity(): int
    {
        return $this->sum(function (Item $item) {
            return $item->quantity();
        });
    }

    public function subtotal(): Money
    {
        /** @TODO: Find a good way to not hard-code the currency type. */
        return new Money(
            $this->sum(function (Item $item) {
                return $item->price()->multiply($item->quantity())->getAmount();
            }),
            new Currency('USD')
        );
    }

    private function throwIf(\Throwable $exception, bool $throw)
    {
        if ($throw) {
            throw $exception;
        }
    }

    private function removeIfFound(string $identifier)
    {
        if ($this->has($identifier)) {
            $this->remove($identifier);
        }
    }
}
