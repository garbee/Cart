<?php

namespace Garbee\Cart\Contracts;

use Garbee\Cart\Collections\Cart as CartCollection;

interface Cart
{
    public function items(): CartCollection;

    public function destroy();
}
