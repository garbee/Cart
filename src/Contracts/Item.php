<?php

namespace Garbee\Cart\Contracts;

use Money\Money;

interface Item
{
    public function identifier(): string;

    public function name(): string;

    public function price(): Money;

    public function quantity(): int;
}
