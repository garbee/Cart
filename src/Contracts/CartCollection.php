<?php

namespace Garbee\Cart\Contracts;

use Money\Money;

interface CartCollection
{
    public function add(Item $item): CartCollection;

    public function remove(string $identifier): CartCollection;

    public function quantity(): int;

    public function subtotal(): Money;
}
