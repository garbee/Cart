<?php

namespace Garbee\Cart\Contracts;

interface SessionInterface
{
    public function key(): string;

    public function instance(): string;

    public function retrieve();

    public function put($value);

    public function exists(): bool;

    public function destroy();
}
