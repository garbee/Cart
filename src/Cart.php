<?php

namespace Garbee\Cart;

use Garbee\Cart\Contracts\Cart as Contract;
use Garbee\Cart\Contracts\SessionInterface;

class Cart implements Contract
{
    /** @var SessionInterface $store */
    private $store;

    /** @var Collections\Cart|null */
    private $current;

    /**
     * Cart constructor.
     * @param SessionInterface $store
     */
    public function __construct(SessionInterface $store)
    {
        $this->store = $store;
    }

    public function store(): SessionInterface
    {
        return $this->store;
    }

    public function items(): Collections\Cart
    {
        if ($this->current) {
            return $this->current;
        }

        if ($this->store->exists()) {
            return $this->current = $this->store->retrieve();
        }

        $cart = (new Collections\Cart());

        $this->store->put($cart);

        return $this->current = $cart;
    }

    public function destroy()
    {
        $this->current = null;
        $this->store->put($this->current);
    }
}
