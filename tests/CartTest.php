<?php

namespace Garbee\Cart\Test;

use Garbee\Cart\Exceptions\ItemNotFound;
use Money\Money;
use Money\Currency;
use Garbee\Cart\Item;

class CartTest extends TestCase
{
    public function testQuantityTotalsCorrect()
    {
        $this->cart->items()->add(new Item(
            'prod_1',
            'Test',
            3,
            new Money(
                400,
                new Currency('USD')
            )
        ));
        $this->cart->items()->add(new Item(
            'prod_2',
            'Test',
            9,
            new Money(
                200,
                new Currency('USD')
            )
        ));

        $this->assertEquals(12, $this->cart->items()->quantity());
    }

    public function testCartItemsCalculatesSubtotal()
    {
        $this->cart->items()->add(new Item(
            'prod_1',
            'Test',
            3,
            new Money(
                200,
                new Currency('USD')
            )
        ));
        $this->cart->items()->add(new Item(
            'prod_2',
            'Test',
            4,
            new Money(
                100,
                new Currency('USD')
            )
        ));

        $this->assertEquals(
            new Money(
                1000,
                new Currency('USD')
            ),
            $this->cart->items()->subtotal()
        );
    }


    public function testThrowsWhenRemovingNonExistentItem()
    {
        $this->expectException(ItemNotFound::class);
        $this->cart->items()->remove('whateva');
    }

    public function testExistingItemIsReplaced()
    {
        $this->cart->items()->add(new Item(
            'prod-1',
            'Test',
            3,
            new Money(100, new Currency('USD'))
        ));
        $this->assertEquals(3, $this->cart->items()->first()->quantity());
        $this->cart->items()->add(new Item(
            'prod-1',
            'Test',
            1,
            new Money(100, new Currency('USD'))
        ));
        $this->assertEquals(1, $this->cart->items()->first()->quantity());
        $this->assertEquals(1, $this->cart->items()->count());
    }
}
