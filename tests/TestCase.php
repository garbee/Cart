<?php

namespace Garbee\Cart\Test;

use Garbee\Cart\Cart;
use Illuminate\Session\Store;
use Illuminate\Filesystem\Filesystem;
use PHPUnit\Framework\TestCase as Base;
use Illuminate\Session\FileSessionHandler;
use Garbee\Cart\Support\Illuminate\Session;

abstract class TestCase extends Base
{
    /** @var Cart $cart */
    protected $cart;

    /** @var Store $store */
    protected $store;

    public function setUp()
    {
        $handler = new FileSessionHandler(new Filesystem(), __DIR__ . '../../storage/sessions', 5);
        $this->store = new Store('garbee_cart_test_session', $handler);
        $session = new Session($this->store, 'cart', 'test');
        $this->cart = new Cart($session);
    }
}
