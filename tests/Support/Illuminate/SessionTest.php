<?php

namespace Garbee\Cart\Test\Support\Illuminate;

use Money\Money;
use Money\Currency;
use Garbee\Cart\Cart;
use Garbee\Cart\Item;
use Illuminate\Session\Store;
use PHPUnit\Framework\TestCase;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Session\FileSessionHandler;
use Garbee\Cart\Support\Illuminate\Session;

class SessionTest extends TestCase
{
    /** @var Cart $cart */
    private $cart;

    /** @var Store $store */
    private $store;

    public function setUp()
    {
        $handler = new FileSessionHandler(new Filesystem(), __DIR__ . '../../storage/sessions', 5);
        $this->store = new Store('garbee_cart_test_session', $handler);
        $session = new Session($this->store, 'cart', 'test');
        $this->cart = new Cart($session);
    }

    public function testItCanGetProperties()
    {
        $this->assertInstanceOf(Session::class, $this->cart->store());

        $this->assertEquals($this->cart->store()->instance(), 'test');
        $this->assertEquals($this->cart->store()->key(), 'cart');

        $this->cart->items()->add(new Item(
            'test_prod_1',
            'Test Product',
            2,
            new Money(
                200,
                new Currency('USD')
            )
        ));

        $this->assertCount(1, $this->cart->items());

        $this->cart->destroy();

        $this->assertNull($this->cart->store()->retrieve());
    }

    public function testInstancesAreIsolated()
    {
        $wishlist = new Cart(new Session($this->store, 'cart', 'wishlist'));
        $wishlist->items()->add(new Item(
            'test_prod_1',
            'Test Product',
            2,
            new Money(
                200,
                new Currency('USD')
            )
        ));

        $this->assertEquals(1, $wishlist->items()->count());
        $this->assertEquals(0, $this->cart->items()->count());
    }
}
